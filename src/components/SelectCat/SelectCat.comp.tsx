import React from 'react';

import {
  Category,
  SubCategory,
  PartName,
} from '../../store/reducers/categoryReducer';

import classes from './SelectCat.module.scss';

interface Props {
  placeholder: string;
  options: Category[] | SubCategory[] | PartName[];
  handler: Function;
  selector?: Function;
  selectorKey?: string;
  keyName?: string;
  required?: boolean;
}

const SelectCat: React.FC<Props> = ({
  placeholder,
  required,
  keyName,
  selectorKey,
  handler,
  selector,
  options,
}) => {
  return (
    <div className={classes.main}>
      <div className={classes.selectComponent}>
        <select
          required={required ? required : false}
          className={classes.selectArrow}
          onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
            handler(
              keyName,
              (event.target as HTMLSelectElement).value,
              selector,
              selectorKey
            )
          }
        >
          <option value={0}>{placeholder}</option>
          {(options as (Category | SubCategory | PartName)[]).map((option) => {
            // FIXME: TODO: This log is for debugging misspelling of 'translation' word in DB
            console.log(option.translations);
            return (
              <option value={option._id} key={option._id}>
                {option.translations['lt']}
              </option>
            );
          })}
        </select>
      </div>
    </div>
  );
};

export default SelectCat;
